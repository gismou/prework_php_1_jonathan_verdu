<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1 PHP</title>
</head>
<body>

    <form method="POST" action="index.php">
        <label>Introduce un número del 1 al 7.</label><br />
        <input name="numero" type="number" />
        <input type="submit" />
    </form>

    <?php

        $arr_dias = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
        
        if(isset($_POST['numero'])){
            if($_POST['numero'] > 0 && $_POST['numero'] < 8){
                $dia = $arr_dias[$_POST['numero']-1];
                echo "<p style='color:green'>El día de la semana es $dia</p>";
            }else{
                echo "<p style='color:red'>El número introducido no es correcto</p>";
            }
        }

    ?>

  
</body>
</html>